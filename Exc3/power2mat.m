n = input("Please enter size (n) of matrix: "); 
x = 2.^[1:n];
mat = reshape(x, 2, n/2); 
mat = mat'; 
disp(mat); 

%{
power2mat
Please enter size (n) of matrix: 20
           2           4
           8          16
          32          64
         128         256
         512        1024
        2048        4096
        8192       16384
       32768       65536
      131072      262144
      524288     1048576
%} 