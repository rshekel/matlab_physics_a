% Q1
A = rand(6);
disp(A);
m = mean2(A); 
m = mean(mean(A));
disp("Mean: " + m);
lower = sum(sum(A<m));
higher = sum(sum(A>m));
disp("lower than mean: " + lower + ". Higher than mean: " + higher);
[sorted_a, sorted_i] = sort(A(:));
disp("lowest quarter:");
disp(sorted_a(1:length(sorted_a)/4) + " at index " + sorted_i(1:length(sorted_a)/4));
disp("highest quarter:");
disp(sorted_a(end-length(sorted_a)/4:end) + " at index " + sorted_i(end-length(sorted_a)/4:end));

% Q2. 
disp("3n+1 problem for 7:")
disp(n3plus1(7));
disp("3n+1 problem for 35:")
disp(n3plus1(35));
disp("3n+1 problem for 85:")
disp(n3plus1(85));

% Q3. 
for i = 200:300
    v = lagrange(i);
    if sum(v.^2) ~= i
        disp("Error!!");
        disp(i);
        break
    end
    if i == 300
        disp("Seems like largrange was right...");
    end
end 

%{
Exc5
    0.7794    0.2147    0.4052    0.9557    0.8046    0.8452
    0.4623    0.1500    0.2380    0.3976    0.6215    0.7892
    0.6407    0.1159    0.8746    0.5695    0.9338    0.5750
    0.4099    0.8195    0.8883    0.7838    0.7119    0.8870
    0.5696    0.8934    0.2567    0.9761    0.5150    0.7742
    0.0092    0.1754    0.5088    0.1616    0.5285    0.4327

Mean: 0.57429
lower than mean: 18. Higher than mean: 18
lowest quarter:
    "0.0091881 at index 6"
    "0.11592 at index 9"
    "0.15002 at index 8"
    "0.1616 at index 24"
    "0.1754 at index 12"
    "0.21468 at index 7"
    "0.23797 at index 14"
    "0.25672 at index 17"
    "0.39756 at index 20"

highest quarter:
    "0.80456 at index 25"
    "0.81949 at index 10"
    "0.84523 at index 31"
    "0.8746 at index 15"
    "0.88701 at index 34"
    "0.88829 at index 16"
    "0.89343 at index 11"
    "0.93378 at index 27"
    "0.95565 at index 19"
    "0.97611 at index 23"

3n+1 problem for 7:
     7    22    11    34    17    52    26    13    40    20    10     5    16     8     4     2     1

3n+1 problem for 35:
    35   106    53   160    80    40    20    10     5    16     8     4     2     1

3n+1 problem for 85:
    85   256   128    64    32    16     8     4     2     1

Seems like largrange was right...
%}