function [a,b,c,d] = lagrange(n)
%LAGRANGE find a,d,c,d such that a^2 + b^2 + c^2 + d^2 = n
%   Detailed explanation goes here
    while 1
        v = randi([0, ceil(sqrt(n))], 1,4);
        if sum(v.^2) == n
            [a, b, c, d] = deal(v);
            break
        end
    end

end

