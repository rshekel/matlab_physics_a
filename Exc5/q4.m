A = magic(3);
logical_primes = isprime(A);

disp("look at this magic!");
disp(A);

x = input("Please input number (1 or 2): "); 

if x == 1
    A(logical_primes)=0;
    disp("no primes:")
    disp(A); 
elseif x == 2
    A(~logical_primes)=0;
    disp("only primes:")
    disp(A); 
else
    disp("invalid unput, exiting.") 
end

%{
q4
look at this magic!
     8     1     6
     3     5     7
     4     9     2

Please input number (1 or 2): 1
no primes:
     8     1     6
     0     0     0
     4     9     0

q4
look at this magic!
     8     1     6
     3     5     7
     4     9     2

Please input number (1 or 2): 2
only primes:
     0     0     0
     3     5     7
     0     0     2
%}