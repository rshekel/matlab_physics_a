function vec = n3plus1(n) 
    vec = [n];
    while n~=1 
        if mod(n,2) == 0
            n = n/2;
        else
            n = 3*n + 1;
        end
        vec = [vec, n];
    end
end
