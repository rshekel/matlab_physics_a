function lin_fit(x, y)
    m = (sum(x.*y) - sum(x)*mean(y)) / (sum(x.*x) - sum(x)*mean(x));
    b = (sum(y) - m*sum(x)) / length(x);
    
    x2 = linspace(1700, 2050, 500);
    y2 = x2.*m + b; 
    plot(x,y, '.');
    hold on;
    plot(x2, y2);
    
    
    
end

