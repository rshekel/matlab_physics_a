function [x_zero, y_zeros] = poly_zeros(pol)
%POLY_ZEROS returns zeros of polynomial, givven by its coefficients 
y_zeros = roots(pol); 
x_zero =  polyval(pol, 0);

end

