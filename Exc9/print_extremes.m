function print_extremes(pol)
der1 = polyder(pol); 
der2 = polyder(der1);

extremes = roots(der1);
% Transpose important, otherwise x will get vector value, instead of
% iterating through the vector.. 
for x = extremes'
    if polyval(der2, x) > 0
        disp(x + " is a minimum");
    else
        disp(x + " is a maximum");
    end
end

end
