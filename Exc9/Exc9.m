%% Matlab - exc9
% Ronen Shekel, 309987493

%% Q1. 
grades = randi(60, 40, 1) + 40;
histogram(grades, 40:10:100); 
title("grades histogram");

%% Q2
p1 = [2, -9, 1, 12]; 
[x_zero, y_zeros] = poly_zeros(p1);
disp("x_zero: " + x_zero); 
disp("y_zeros: " + y_zeros); 

%% Q3 
print_extremes(p1);

%% Q4 
load census;
lin_fit(cdate, pop);
disp("doesn't seem linear...");

%% poly_zeros.m
% <include>poly_zeros.m</include>


%% print_extremes.m
% 
% <include>print_extremes.m</include>
% 

%% lin_fit.m
% 
% <include>lin_fit.m</include>
% 
