function m = monteCarloF(n)
x = rand(n, 1); 
y = rand(n, 1); 
z = rand(n, 1); 
f = tripint(x, y, z); 
m = mean(f); 
end

