function f = tripint(x, y, z)
    f = 1./(x.*y.*z).^(x.*y.*z); 
end
