% Q1 
mat1 = get_mat(4, 5, true, 10, 40);
mat2 = get_mat(3, 5, true, 10, 40);

% Q2. 
v1 = [1 4 9 16 25 36 49];
v2 = [1 8 27 64 125 216 343];
v3 = [1 1 1 1 1 1 1];
v4 = [1 2 3 4 5 98 102]; 
v5 = [1 2 4 8 16 32 64]; 
val1 = is_diffable(v1);
val2 = is_diffable(v2);
val3 = is_diffable([v1; v2; v3; v4; v5]);

% Q3.
fix_triangle(3, 4, 5);
fix_triangle(2, 5, 10);

% Q4. 
[r, a] = rect2polar(3,5); 

% Q5. 
[x,y] = rect2polar(6, pi/5);

% Q6. 
i = monteCarloF(1e6);


%{ 
% Q1. 
get_mat(4, 5, true, 10, 40)
there aren't enough values in range for these dimensions

get_mat(3, 5, true, 10, 40)

ans =

    20    30    18    34    32
    36    12    40    10    26
    16    24    28    14    22

% Q2. 

val1 =

     2

val2 =

     6

val3 =

     2
     6
     1
     0
     0

Q3. 
fix_triangle(3, 4, 5);
fix_triangle(2, 5, 10);
triangle is OK
triangle won't work. try using 6 instead of 10

Q4.
[r, a] = rect2polar(3,5); 
r

r =

    5.8310

a

a =

    1.0304

Q5. 
[x,y] = rect2polar(6, pi/5);
[x,y]

ans =

    6.0328    0.1043

Q6. 
monteCarloF(1e6)

ans =

    0.8350
%} 
