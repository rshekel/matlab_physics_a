function answers = is_diffable(A)

s = size(A);
answers = zeros(s(1), 1); % amount of rows 

for i = 1:length(answers)
    v = A(i, :); 
    while true
        if isempty(v) 
            break % answers(i) is already set to zero 
        end
        u = diff(v);
        if ~isempty(u) && ~any(u) % next iteration is all zeros 
            answers(i) = v(1);
            break; 
        end 
        v = u;
    end
end

end

