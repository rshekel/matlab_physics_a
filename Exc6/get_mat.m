function mat = get_mat(n, m, even_only, p, q)
% For Q1 

if nargin <= 3
    p = 0;
end

if nargin <= 4
    q = 100;
end

if even_only 
    if mod(p, 2) == 1
        p = p+1; 
    end 
    nums = p:2:q;
else
    nums = p:q;
end

if n*m > length(nums)
    disp("there aren't enough values in range for these dimensions");
    return 
end
    
nums = nums( randperm( numel(nums) ) ); % now in random order 
nums = nums(1:n*m);

mat = reshape(nums, n, m);
end

