function [x,y] = polar2rect(r,a)
x = r*cos(a);
y = r*sin(a); 
end

