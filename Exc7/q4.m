function q4(pol1, pol2)
    per1 = perimeter(pol1); 
    per2 = perimeter(pol2); 
    disp(per1);
    disp(per2);
    f = figure; 
    if per1 > per2 
        plot(pol1(1, :), pol1(2, :));
    else
        plot(pol2(1, :), pol2(2, :));
    end
    title("Q4");
    saveas(f, './images/4.png');    
end
