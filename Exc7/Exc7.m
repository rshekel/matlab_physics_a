%% Matlab - exc7
% Ronen Shekel, 309987493

%% Q1. 
x = linspace(0, 2*pi, 200);
f = figure; 
plot(x, sin(x.^2) - cos(1./x.^2));
title("1-a: sin(x^2) - cos(1/x^2)"); 
saveas(f, './images/1-a.png');

f = figure; 
plot(x, x./(exp(x)+exp(-x))); 
title("1-b: x/(e^x+e^{-x})");
saveas(f, './images/1-b.png');

f = figure; 
x = linspace(0.02, 2, 100); 
f = plot(x, x.^3+5*x.^2-18*x+9 - 1./x + 2./(x.^2)); 
title("1-c: x^3+5*x^2-18*x+9 - 1/x + 2/(x^2)");
saveas(f, './images/1-c.png');


%% Q2. 
x = linspace(0, 2*pi, 200);
f = figure; 
plot(x, sin(x.^2) - cos(1./x.^2));
title("2-a: sin(x^2) - cos(1/x^2) and 1-b: x/(e^x+e^{-x})"); 
hold on; 
plot(x, x./(exp(x)+exp(-x))); 
saveas(f, './images/2-a.png');

f = figure; 
plot(x, sin(x.^2) - cos(1./x.^2), x, x./(exp(x)+exp(-x)));
title("2-b: sin(x^2) - cos(1/x^2) and 1-b: x/(e^x+e^{-x})"); 
saveas(f, './images/2-b.png');

%% Q3. 
f = figure;
x = [0 5*pi]; 
fplot("3+sin(pi*x)", x); 
hold on;
fplot("x/2 +1", x); 
fplot("(log(x+1))^3", x); 
title("Q3");
saveas(f, './images/3.png');

%% Q4
pol1 = [1 5 7 2 1; 
        2 1 4 6 2 ];

pol2 = [1 5 13 2 1; 
        2 1 4 1 2];

q4(pol1, pol2);

%{
Q4 Perimeters. 
17.2369
25.4831
%}

%% dist.m
% <include>dist.m</include>


%% perimeter.m
% 
% <include>perimeter.m</include>
% 

%% q4.m
% 
% <include>q4.m</include>
% 
