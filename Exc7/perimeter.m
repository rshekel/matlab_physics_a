function p = perimeter(pol)
    points = pol';
    p = 0; 
    for i = 1:length(points)-1
        p = p + dist(points(i,:), points(i+1, :));
    end
        
end

