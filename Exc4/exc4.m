% Q1 a
syms x y z u;
eq1 = 16*x+2*y+3*z+13*u==4;
eq2 = 5*x+11*y+10*z+8*u==2;
eq3 = 9*x+7*y+6*z+12*u==3;
eq4 = 4*x+14*y+15*z+u==1 ;
[A,B] = equationsToMatrix([eq1, eq2, eq3, eq4], [x, y, z, u]);

sol1a = A\B; 
% solution is not unique, because equations are rank deficient, so there
% is an infinite number of solutions or no solution. specifically here - an
% infinite amount 
%{
rref([A B])
 
ans =
 
[ 1, 0, 0,  1, 37/136]
[ 0, 1, 0,  3, 57/136]
[ 0, 0, 1, -3, -27/68]
[ 0, 0, 0,  0,      0]
%}

% Q1 b 
eq1 = -2*x+2*y+3*z-u==1;
eq2 = 5*x-6*y-5*z+8*u==3;
eq3 = 9*x+7*y+6*z+12*u==2;
eq4 = 4*x-4*y-3*z+u==4;
[A,B] = equationsToMatrix([eq1, eq2, eq3, eq4], [x, y, z, u]);

sol1b = A\B; 
% single solution: 
%{
rref([A, B])
[ 1, 0, 0, 0,   453/692]
[ 0, 1, 0, 0, -1277/692]
[ 0, 0, 1, 0,  1335/692]
[ 0, 0, 0, 1,  -147/692]
%}

% Q2. 
syms a b c d 
syms t w p r 
syms e f g h 
sol2a = [a b c d] * [x;y;z;u];
sol2b =  [x;y;z;u]*[a b c d];
sol2c = [a b c d] * [t*x;w*y;p*z;r*u];
sol2d = [a b c d; e f g h] * [x;y;z;u];
sol2e = [a b c d; e f g h] * [x*t;y*w;z*p;u*r];

% Q3. 
n = [1 10 100 500 1000 2000 4000 8000];
y = (1+1./n).^n;
e = exp(1);

% Q4. 
n1 = 0:50;
n2 = 0:500;
n3 = 0:5000;
v1 = 1./((2*n1+1).*(2*n1+2));
v2 = 1./((2*n2+1).*(2*n2+2));
v3 = 1./((2*n3+1).*(2*n3+2));
s1 = sum(v1);
s2 = sum(v2);
s3 = sum(v3);
sol4 = [s1 s2 s3 log(2)];

% Q5. 
A = [5 2 4 ; 1 7 -3 ; 6 -10 0];
B = [11 5 -3 ; 0 -12 4 ; 2 6 1];
C = [7 14 1; 10 3 -2; 8 -5 9];
sol5a = A+B==B+A; % equal
sol5b = A+(B+C) == (A+B)+C; % equal
sol5c = 5*(A+C) == 5*A + 5*C; % equal
sol5d = A*(B+C) == A*B + A*C; % equal

%Q6. 
sol6a = A*B == B*A; % not equal 
sol6b = A*(B*C) == (A*B)*C; % equal 
sol6c = (A*B)' == B'*A'; % equal 
sol6d = (A+B)' == A' + B'; % equal 


%{
Q1. 
sol1a =
 
 37/136
 57/136
 -27/68
      0

sol1b =
 
   453/692
 -1277/692
  1335/692
  -147/692

Q2. 

sol2a =
 
a*x + d*u + b*y + c*z
  
sol2b =
 
[ a*x, b*x, c*x, d*x]
[ a*y, b*y, c*y, d*y]
[ a*z, b*z, c*z, d*z]
[ a*u, b*u, c*u, d*u]
  
sol2c =
 
d*r*u + a*t*x + c*p*z + b*w*y
  
sol2d =
 
 a*x + d*u + b*y + c*z
 e*x + h*u + f*y + g*z
  
sol2e =
 
 d*r*u + a*t*x + c*p*z + b*w*y
 h*r*u + e*t*x + g*p*z + f*w*y
 
Q3. 
y =

    2.0000    2.5937    2.7048    2.7156    2.7169    2.7176    2.7179    2.7181

e =

    2.7183

Q4. 
sol4 =

    0.6883    0.6926    0.6931    0.6931

Q5. 
sol5a =

  3�3 logical array

   1   1   1
   1   1   1
   1   1   1

sol5b =

  3�3 logical array

   1   1   1
   1   1   1
   1   1   1

sol5c =

  3�3 logical array

   1   1   1
   1   1   1
   1   1   1

sol5d =

  3�3 logical array

   1   1   1
   1   1   1
   1   1   1

Q6.
sol6a =

  3�3 logical array

   0   0   0
   0   0   0
   0   0   0

sol6b =

  3�3 logical array

   1   1   1
   1   1   1
   1   1   1

sol6c =

  3�3 logical array

   1   1   1
   1   1   1
   1   1   1

sol6d =

  3�3 logical array

   1   1   1
   1   1   1
   1   1   1

%} 