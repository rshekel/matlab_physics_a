c = input("Please enter temperature in Celsius: ");
f = (9/5)*c + 32;
disp("Temperature in fahrenheit is: " + f);

%{ 
% example output:
t8
Please enter temperature in Celsius: 100
Temperature in fahrenheit is: 212
t8
Please enter temperature in Celsius: 0
Temperature in fahrenheit is: 32
%}
