format short g

% 1-a
x = (2+7)^3 + (273^(2/3))/2 + (55^2)/3;
disp("1-a:  " + x);

% 1-b
x = 2^3 + 7^3 + (273^3)/2 + 55^(3/2);
disp("1-b:  " + x);

% 2-a
e = exp(1);
x = cos((5*pi)/6)^2 * sin(((7*pi)/8)^2) + (tan((pi/6)*log(8)))/sqrt(7);
disp("2-a:  " + x);

% 2-b
x = cos(((5*pi)/6)^2) * sin((7*pi)/8)^2 + (tan((pi*log(8))/6))/(7*(5/2));
disp("2-b:  " + x);

x = 13.5;
% 3-a
a = x^3 + 5*x^2 -26.7*x - 52;
disp("3-a:  " + a);

% 3-b
a = (sqrt(14*x^3))/exp(3*x);
disp("3-b:  " + a);

% 3-c
a = log10(abs(x^2-x^3));
disp("3-c:  " + a);

x = (5/24)*pi;
threshold = 1e-15;
% 4-a 
a = sin(2*x);
b = 2*sin(x)*cos(x); 
% not using "==" because of floating point comparison issues
c = abs(a - b) < threshold;
disp("4-a: a==b: " + c);

% 4-b
a = cos(x/2);
b = sqrt((1+cos(x))/2);
c = abs(a - b) < threshold;
disp("4-b: a==b: " + c);

x = (3/17)*pi;
%5-a
a = tan(2*x);
b = (2*tan(x))/(1-(tan(x)^2));
c = abs(a - b) < threshold;
disp("5-a: a==b: " + c);

% 5-b
a = tan(x/2);
b = sqrt((1-cos(x))/(1+cos(x)));
c = abs(a - b) < threshold;
disp("5-b: a==b: " + c);

% 6
A = 3;
B = 5; 
C = -6;
x0 = 2;
y0 = -3;
d = (abs(A*x0 + B*y0 + C))/(sqrt(A^2 + B^2));
disp("6: d=" + d);

% 7 
M2 = 7.2;
M1 = 5.3;
E_ratio = 10^((M2 - M1)*(3/2));
disp("7: E2/E1=" + E_ratio);

%{ 
output:
exc1
1-a:  1758.3749
1-b:  10173967.3909
2-a:  1.4395
2-b:  0.23246
3-a:  2959.175
3-b:  4.7823e-16
3-c:  3.3576
4-a: a==b: true
4-b: a==b: true
0-a: a==b: true
5-b: a==b: true
6: d=2.5725
7: E2/E1=707.9458
%}
