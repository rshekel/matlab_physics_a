%% Matlab - exc8
% Ronen Shekel, 309987493

%% Q1. 
s1 = shoot_line([1,1], [3,3], 1)

%% Q2. 
two_shooters([2,2], [11,15])

%% Q3. 
x = [-2*pi 2*pi];
f1 = @(x) sin(x.^2)-cos(1./x.^2);
f2 = @(x) x./(exp(x)+exp(-x));
f3 = @(x) x.^3+5*x.^2-18*x+9-1./x+2./(x.^2);

f = figure;
subplot(3, 1, 1);
fplot(f1, x);
subplot(3, 1, 2);
fplot(f2, x);
subplot(3, 1, 3);
fplot(f3, x);
% saveas(f, './images/q3a.png');

f = figure;
subplot(3, 1, 1);
ezplot(f1, x);
subplot(3, 1, 2);
ezplot(f2, x);
subplot(3, 1, 3);
ezplot(f3, x);
% saveas(f, './images/q3b.png');


%% Q4. 
f = figure; 
a = sqrt(4);
b = sqrt(5);
t = linspace(-2*pi, 2*pi, 100);
x = a*cos(t);
y = b*sin(t);
plot(x, y); 
% saveas(f, './images/q4.png');

%% Q5. 
figure; 
x = linspace(-10, 10, 1000);
y = 2*x.^3 -9*x.^2 + x +12;
plot(x, y);
% [a,b] = ginput(4);
%{ 
it is hard to point exactly on axis in this resolution...
a = 
    -0.0000
    -1.0753
    1.4747
    4.0092
b =
   11.8677
   -0.9728
   -0.9728
   -0.9728
 
%}

%% Q6. 
monte_carlo_pi_simulation

%% two_shooters.m
% 
% <include>two_shooters.m</include>
% 

%% shoot_line.m
% 
% <include>shoot_line.m</include>
% 

%% monte_carlo_pi_simulation.m
% 
% <include>monte_carlo_pi_simulation.m</include>
% 
