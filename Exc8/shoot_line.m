function ret = shoot_line(cs, ct, rt)
x1 = cs(1);
y1 = cs(2);

x2 = randi(20);
y2 = randi(20);

x0 = ct(1);
y0 = ct(2);

d = abs((x2-x1)*(y1-y0)-(x1-x0)*(y2-y1)) / sqrt( (x2-x1)^2+(y2-y1)^2 );

if d < rt
    ret = 1;
    viscircles(ct, rt); 
    axis([0 20 0 20]);
    hold on;
    plot([x1, x2], [y1,y2]); 
else
    ret = 0;

end

