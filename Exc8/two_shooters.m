function winner = two_shooters(cs, ct)

figure;
viscircles(cs, 2); 
viscircles(ct, 2); 
axis([0 20 0 20]);
hold on;

x = randi(2); 
if x == 1
    cs_first = true;
else
    cs_first = false;
end

while true 
    if cs_first
        if shoot_line(cs, ct, 2) 
            winner = 1;
            break
        end 
        if shoot_line(ct, cs, 2) 
            winner = 2;
            break
        end  
    else
        if shoot_line(ct, cs, 2) 
            winner = 2;
            break
        end 
        if shoot_line(cs, ct, 2) 
            winner = 1;
            break
        end 
    end
end

end

