n = 50000;
x = rand(n, 1);
y = rand(n, 1);

ri = x.^2 + y.^2 <= 1;
red = [x(ri), y(ri)];
blue = [x(~ri), y(~ri)];

mc_pi = length(red) / n * 4;
disp("mc_pi: " + mc_pi);
disp("err: " + abs(pi-mc_pi) / pi);

close all;
f = figure;
plot(red(:, 1), red(:, 2), 'r.');
hold on;
plot(blue(:, 1), blue(:, 2), 'b.');
% saveas(f, './images/q6.png');
