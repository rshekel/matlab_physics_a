function [ok, col, row] = f1(A,v)
% F1(A,v) is a function that compares between an input row % vector v and matrix A. 
%{
Running example from the Command Window: 
A = [ 1 2 3 ; 4 5 6 ; 7 8 9 ; 1 4 7 ]
v = [ 3 6 9 7 ]
[ok  , col, row] = f1(A,v)
%}
% results:
% ok =  1 ; col =  3 ; row =  -1

% make sure it is a row vector 
v = reshape(v', 1, length(v));

ok = false;
col = -1;
row = -1;

[row_n, col_n] = size(A); 

if length(v) == row_n 
    for i = 1:col_n
        if A(:, i) == v'
            col = i;
            ok = true;
        end
    end
end

if length(v) == col_n 
    for i = 1:row_n
        if A(i, :) == v
            col = row;
            ok = true;
        end
    end
end

end