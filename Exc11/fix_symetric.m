function good_mat = fix_symetric(bad_mat)

disp(bad_mat); 

bad_i = find(diag(bad_mat) ~= -1, 1);
if isempty(bad_i)
    bad_i = size(bad_mat, 1);
end

new_c = zeros(size(bad_mat, 1), 1); 

temp_mat = [bad_mat(:, 1:bad_i-1) new_c bad_mat(:, bad_i:end)];

sz = size(temp_mat, 1);
for i = [1:sz]
    if i == bad_i
        new_c(i) = -1;
    else
        new_c(i) = temp_mat(bad_i, i);
    end
    
end

good_mat = [temp_mat(:, 1:bad_i-1) new_c temp_mat(:, bad_i+1:end)]

end
