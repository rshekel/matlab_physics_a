%% Matlab - exc11
% Ronen Shekel, 309987493

%% Q1. 
syms y;
dsolve('64*D2y +112*Dy + 51*y == 0')

%% Q2 
syms y(t);
Dy = diff(y);
D2y = diff(Dy); 
dsolve(8*D2y + 14*y == 0, y(0) == 9, Dy(0) == 5)

%% Q3 
readtable("stat.xlsx");
max_sum = max(stat.PAID);
i = find(stat.PAID == max_sum);
disp("Account details of max paid account: ");
stat(i, :)

max_year = stat.YEAR(i);
good_indexes = find(stat.YEAR == max_year); 
new_table = stat(good_indexes, :);

months = 1:12;
indexes_of_month = @(i) find(new_table.MONTH == i);
sum_of_month = @(i) sum(new_table.PAID(indexes_of_month(i)));

sums_of_months = arrayfun(sum_of_month, 1:12);
CumlutiveSum = cumsum(sums_of_months);
CumlutiveSum = [CumlutiveSum, nan(1, length(new_table.MONTH) - 12)];
CumlutiveSum = CumlutiveSum';
final_table = addvars(new_table, CumlutiveSum);
writetable(final_table, "stat_year.xlsx");
disp("final table:");
disp(final_table);

%% Q4 
bm = [-1, 2, 4; 
      2, -1, 6; 
      3, 5 7; 
      4, 6, -1];
  
bm2 = [1, 2, 4; 
      -1, 2, 6; 
       3, -1, 7; 
       4, 6, -1];

fix_symetric(bm);

%% Q5 
A = [ 1 2 3 ; 4 5 6 ; 7 8 9 ; 1 4 7 ];
v = [ 3 6 9 7 ];
[ok  , col, row] = f1(A,v)

%% f1.m
% 
% <include>f1.m</include>
% 

%% fix_symetric.m
% 
% <include>fix_symetric.m</include>
% 
