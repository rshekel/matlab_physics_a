%% Matlab - exc10
% Ronen Shekel, 309987493

%% A-1. 
syms x;
sol = solve(x^3 -7*x -2, 'Real', true);
double(sol)

%% A-2 
syms x;
sol = solve(cos(2*x) - sin(x/2)/tan(x) == 0, 'Real', true);
double(sol)

xs = linspace(-2*pi, 2*pi, 200);
ys = cos(2.*xs) - sin(xs./2)./tan(xs);
plot(xs, ys);
hold on
plot(double(sol), zeros(1, length(sol)), 'X');

%% A-3 
syms y;
dsolve("64*D2y + 112*Dy + 51*y == 0")

%% A-4 
syms y;
dsolve('8*D2y +14*y == 0',  'y(0) = 9', 'Dy(0) = 5') 

%% B 
syms x
assume(x>5);
f = (x+3)^3 - exp(x/2);
min_x = solve(diff(f)==0); 
y_min = subs(f, x, min_x);

figure; 
fplot(f, [-20, 21]);
hold on;
plot(min_x, y_min, '*');
